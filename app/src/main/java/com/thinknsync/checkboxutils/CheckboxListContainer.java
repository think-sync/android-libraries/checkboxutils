package com.thinknsync.checkboxutils;

import java.util.List;

public interface CheckboxListContainer<T> {
    void addToCheckedList(T object);
    void removeFromCheckedList(T object);
    void removeFromCheckedList(int index);
    List<T> getCheckedItems();
}
