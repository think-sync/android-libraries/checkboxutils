package com.thinknsync.checkboxutils;

import java.util.ArrayList;
import java.util.List;

public class CheckboxListContainerImpl<T> implements CheckboxListContainer<T> {

    protected List<T> checkboxHostList = new ArrayList<>();

    @Override
    public void addToCheckedList(T object) {
        checkboxHostList.add(object);
    }

    @Override
    public void removeFromCheckedList(T object) {
        checkboxHostList.remove(object);
    }

    @Override
    public void removeFromCheckedList(int index) {
        checkboxHostList.remove(index);
    }

    @Override
    public List<T> getCheckedItems() {
        return checkboxHostList;
    }
}
